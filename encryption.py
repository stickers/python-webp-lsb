
from cryptography.fernet import Fernet
import sys 
import binascii

def encrypt():
    
    f = open(str(sys.argv[2]),"r")
    text = f.read()
    f.close()

    key = Fernet.generate_key()
    print("Your key is: ",key)
    text2 = open("key.txt",'wt')
    n = text2.write(key.decode("utf-8"))
    fe = Fernet(key)
    token = fe.encrypt(binascii.a2b_qp(text))
    print("Your token has been created: ", token)
    # secret = fe.decrypt(token)
    # print(secret.decode("utf-8"))
    text_file = open("token.txt", "wt")
    n = text_file.write(token.decode("utf-8"))
    text_file.close()


def decrypt():
    reading = open("key.txt")
    key = reading.read()  
    fe = Fernet(key)
    reader2 = open ("token.txt", "r")
    token = reader2.read()
    realtoken = token.encode()
    secret =fe.decrypt(realtoken)
    print("Your secret is: ",secret.decode("utf-8"))

    # secret = fe.decrypt(binascii.b2a_qp(token))
    # # print("this is secret:", secret)
    # # secret = fe.decrypt(token)
