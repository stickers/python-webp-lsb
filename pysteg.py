from PIL import Image
import sys
import encryption
import os
import os.path



def pad(text,image):
    capacity= (3 * image.width * image.height)
    print('capacity is: ' + str(capacity))


    while len(text) != capacity:
        text += str(0)

    return text


def encode(mode):
    

    f = open(str(sys.argv[2]), "r")  # message to hide
    text = f.read()
    f.close()

    if len(text) == 0:  # message without characters
        return 0
        
    if (mode.lower() == 'bulk'): #check for the mode of operation
        inputdir = readfromconfig('inputdir')
        image = Image.open(os.path.join(inputdir, sys.argv[1]))  
    else:
        image = Image.open(sys.argv[1])

    # Value of every character of the hidden message in binary expressed in 8 bits + bits of control
    # Control bits are 0 if is not the last character, otherwise it is 1.
    binary = ""  # string of hidden message in binary + control bits.
    #text = pad(text,image)
    for i in range(len(text)):
        binary = binary + bin(ord(text[i]))[2:].zfill(8)
        if i == len(text)-1:
            binary = binary + "1"  # control bit set to 1 (last case)
        else:
            binary = binary + "0"  # control bit set to 0

    pix = image.load()
    if image.mode == "RGB":  # image has RGB pixels
        rgba = 0
    else:
        rgba = 1  # image has RGBA pixels


    width, height = image.size  # size of the image
    print("before:",len(text))
    print("after:",len(text))
    if height*width*3 < len(text):
        print("your file is too large")
        print("Your file size:",len(text))
        print("Your image LSB capacity:",3*image.height*image.width)
        quit()
    count_letter = 0  # number of the letter of the message
    for i in range(0, width):
        for j in range(0, height):
            if rgba == 1:
                RGBA = image.getpixel((i, j))  # current pixel
                R, G, B, A = RGBA
            else:
                RGB = image.getpixel((i, j))  # current pixel
                R, G, B = RGB

            
            if bin(R)[2:].zfill(8)[7:] == "0":  # lsb of the component is 0
                R = R | int(binary[count_letter])
            else: #LSB OF COMPONENT IS 1

                if int(binary[count_letter]) == 0:  # the digit of the message is 0
                    R = R & int(254)
                if bin(R)[2:].zfill(8) == "11111111": # NEW: Omitting changes to a solid color RED
                     R = R
                     #print(f"The red pixel at {i},{j} is {R}")
                else: #the digit of the message is 1 
                    R = R #& int(255)
            count_letter = count_letter + 1

            if bin(G)[2:].zfill(8)[7:] == "0":
                G = G | int(binary[count_letter])
            else:             
                if int(binary[count_letter]) == 0:
                    G = G & int(254)
                if bin(G)[2:].zfill(8) == "11111111": # NEW: Omitting changes to a solid color GREEN
                     G = G 
                else:
                    G = G & int(255)
            count_letter = count_letter + 1
            if bin(B)[2:].zfill(8)[7:] == "0":
                B = B | int(binary[count_letter])
            else:
               
                if int(binary[count_letter]) == 0:
                    B = B & int(254)
                if bin(B)[2:].zfill(8) == "11111111": # NEW: Omitting changes to a solid color BLUE
                     B = B 
                else:
                    B = B & int(255)
            count_letter = count_letter + 1
            # now we set the current pixel with the new value
            if rgba == 1:
                pix[i, j] = eval(
                    "(" + str(R) + ", " + str(G) + ", " + str(B) + ", " + str(A) + ")")
            else:
                pix[i, j] = eval(
                    "(" + str(R) + ", " + str(G) + ", " + str(B) + ")")

            # we arrived to the end of the message
            if count_letter == len(binary):
                break
        if count_letter == len(binary):
            break

    filename = sys.argv[1]
    filenamefx = sys.argv[1].split('.')
    # we are here
    outputdir = readfromconfig('outputdir')
    outputdirectory = os.path.abspath(
        outputdir)
    filename = filenamefx[0] + '_modified' + '.png'

    finaldirectory = outputdirectory + '/' + filename

    image = image.convert('RGBA')
    image.save(finaldirectory)  # new image with the hidden message


    

def split(input, size):  # splits a string in n digits
    return [input[start:start+size] for start in range(0, len(input), size)]


def decode():
    # image = Image.open(sys.argv[2]) # image to decode the hidden message
    image = Image.open(sys.argv[2])
    pix = image.load()
    width, height = image.size  # size of the image
    pixel_count = 1  # every 3 pixels we check control bit
    string = ""  # will be the result
    end = 0
    for i in range(0, width):
        if end == 1:  # we finished (control bit = 1)
            break
        for j in range(0, height):
            if image.mode == "RGBA":
                RGBA = image.getpixel((i, j))
                R, G, B, A = RGBA
            else:
                RGB = image.getpixel((i, j))
                R, G, B = RGB
            # append the lsb of the component
            string = string + str(bin(R)[2:].zfill(8)[7:])
            # append the lsb of the component
            string = string + str(bin(G)[2:].zfill(8)[7:])
            if pixel_count < 3:  # B doesn't have a control bit
                string = string + str(bin(B)[2:].zfill(8)[7:])
                pixel_count = pixel_count + 1
            else:  # B has a control bit
                if str(bin(B)[2:].zfill(8)[7:]) == "1":  # we finished the message
                    end = 1
                    break
                else:  # we still have message to extract
                    pixel_count = 1
    listing = split(string, 8)
    # print the result with ASCII characters

    print("Your secret is:")
    print(''.join(str(chr(int(i, 2))) for i in listing))
    


def encodeprocess(inputfile):
    mode = 'bulk'
    if inputfile == 'tbd':
        inputfile = sys.argv[1]
        mode = 'shell'
    if len(sys.argv) >= 2:
        if sys.argv[1] == "--h":
            if len(sys.argv) == 2:
                print(
                    "1. Usage: To encode a message: pysteg.py [original image] [text file]")
                print(
                    "2. Usage: To decode the message: pysteg.py --decode [modified image]")
                print("3. Usage: to encrypt the secret: pysteg.py --encrypt message.txt")
                print("4. Usage: to decrypt the secret: pysteg.py --decrypt token.txt")
            else:
                print(
                    "5. The input is not correct. Use the option --h to see the available options.")
        elif sys.argv[1] == '--encrypt':
            encryption.encrypt()
        elif sys.argv[1] == '--decrypt':
            encryption.decrypt()
        elif sys.argv[1] != "--h" and sys.argv[1] != "--decode":
            if len(sys.argv) != 3:
                print(
                    "The input is not correct. Use the option --h to see the available options.")
            else:
                if encode(mode) != 0:
                    print('***************************************')
                    print(
                        f"Congratulations, the image {inputfile.split('.')[0]}_modified.{inputfile.split('.')[1]} was created with the hidden message.")
                else:
                    print("The text file is empty. or too big")
        elif sys.argv[1] == "--decode":
            if len(sys.argv) != 3:
                print(
                    "The input is not correct. Use the option --h to see the available options.1")
            else:
                decode()

        else:
            print(
                "The input is not correct. Use the option --h to see the available options.")
    else:
        print("The input is not correct. Use the option --h to see the available options.")


# Newly added functions


def startoperation(inputfile, messagefile):
    sys.argv = ["pysteg.py", inputfile, messagefile]
    print('input file: ' + inputfile)
    encodeprocess(inputfile)


def walkthroughfiles(path, messagefile):
    for root, dirs, files in os.walk(path):
        for file in files:
            if file.endswith('.webp'):
                working_directory = path
                file_path = working_directory + str(file)
                filepath = os.path.join(root, file)
                print('filepath:  ' + filepath)
                startoperation(file, messagefile)


def readfromconfig(section):
    configstream = open("config.txt", "r")
    lines = configstream.readlines()
    configstream.close()

    if section.lower() == 'inputdir':
        inputdir = lines[1].replace('Inputdir:', '')
        inputdir = inputdir.replace('\n', '')
        return inputdir

    elif section.lower() == 'outputdir':
        outputdir = lines[2].replace('Outputdir:', '')
        outputdir = outputdir.replace('\n', '')
        return outputdir


def startapp():

    configstream = open("config.txt", "r")
    lines = configstream.readlines()
    configstream.close()
    bulkmodestat = lines[0].split(':')[1]
    bulkmodestat = bulkmodestat.replace('\n', '')

    if bulkmodestat.lower() == 'on':
        messagefile = lines[3].split(':')[1].replace('\n', '')

        pathforwalkthrough = lines[1].replace('Inputdir:', '')
        pathforwalkthrough = pathforwalkthrough.replace('\n', '')
        print('walk through path' + pathforwalkthrough)
        walkthroughfiles(pathforwalkthrough, messagefile)

    else:

        print('Shell mode is on. Bulkmode is off.')
        encodeprocess('tbd')


startapp()
