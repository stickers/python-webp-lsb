PRNG Steganography hides data such as text in an image using LSB and cryptographycally secure pseudo-random number generator to determine the coordinates of pixels to use.

### How it works

SteganoPy3 replaces LSB of each colour component of each pixels with a bit of data to hide until all bits are hidden. 32 first bits are used to write the total length to recover so it can hide a maximum of X * Y * 3 - 32 bits with X and Y the pixels width and length of the image. 

### Cryptography

The key used as seed is derivated with 1M iterations of PBKDF2 with HMAC and SHA-256. 
ChaCha20 with 8 bytes nonce encrypts a counter which is used as PRNG.

### Installation of needed libraries
```
python3 -m pip install --user -r requirements.txt
```
### Running
<strong>Works with PNG images only !</strong>

To hide :
```
PRNGStego.py hide <image.png> -f <data.file> <password> [-o <image_with_data.png>]
```

To recover data:
```
PRNGStego.py show <image_with_data.png> <password> [-o <data.file>]
